package main.lib.personas;

import main.complementos.Guion;

/**
 *Esta clase sirve para declarar los atributos de los actores que actuan en las peliculas de la plataforma EscomFLIX
 * @author osval
 */
public class Actor {
    //Campos de la clase
    private Guion guion;
    private String datosPelicula;
    private String nombreActor;

    /**
     *Constructor para los actores
     * @param guion Objeto que contiene la linea con la cual se identifica el actor
     * @param datosPelicula datos de la pelicula en la que aparece el actor
     * @param nombreActor nombre del actor que aparece en la pelicula
     */
    public Actor( Guion guion, String datosPelicula, String nombreActor ) {
        this.guion = guion;
        this.datosPelicula = datosPelicula;
        this.nombreActor = nombreActor;
    } //Cierre del constructor

    /**
     *Metodo que devuelve el guion (objeto) que usa el actor
     * @return guion Objeto que contiene la linea con la cual se identifica el actor
     */
    public Guion getGuion() {
        return guion;
    }

    /**
     *Metodo que establece el guion (objeto) que usa el actor
     * @param guion Objeto que contiene la linea con la cual se identifica el actor
     */
    public void setGuion( Guion guion ) {
        this.guion = guion;
    }

    /**
     *Metodo que devuelve los datos de la pelicula (String) en la que actua el actor
     * @return datosPelicula datos de la pelicula en la que aparece el actor
     */
    public String getDatosPelicula() {
        return datosPelicula;
    }

    /**
     *Metodo que establece los datos de la pelicula (String) en la que actua el actor
     * @param datosPelicula datos de la pelicula en la que aparece el actor
     */
    public void setDatosPelicula( String datosPelicula ) {
        this.datosPelicula = datosPelicula;
    }

    /**
     *Metodo que devuelve el nombre (String) del actor
     * @return nombreActor nombre del actor que aparece en la pelicula
     */
    public String getNombreActor() {
        return nombreActor;
    }

    /**
     *Metodo que establece el nombre (String) del actor
     * @param nombreActor nombre del actor que aparece en la pelicula
     */
    public void setNombreActor( String nombreActor ) {
        this.nombreActor = nombreActor;
    }

    /**
     *Constructor (sobrecarga) para el actor, que solo necesita del guion y datos de la pelicula
     * @param guion Objeto que contiene la linea con la cual se identifica el actor
     * @param datosPelicula datos de la pelicula en la que aparece el actor
     */
    public Actor( Guion guion, String datosPelicula ) {
        this.guion = guion;
        this.datosPelicula = datosPelicula;
    }//Cierre del constructor

}//Cierre de la clase
