package main.complementos;

/**
 *En esta clase se declaran los atributos para el guion de los actores que actuan en las peliculas de la plataforma EscomFLIX
 * @author osval
 */
public class Guion {
    //Campo de la clase
    private String frase;

    /**
     *Constructor de la frase
     * @param frase Es el String usado para la frase
     */
    public Guion( String frase ) {
        this.frase = frase;
    } //Cierre del constructor

    /**
     *Devuelve los elementos dentro de la cadena de la frase
     * @return frase Es el String usado para la frase
     */
    public String getFrase() {
        return this.frase;
    }//Cierre del m�todo

    /**
     *Establece los elementos dentro de la cadena de la frase
     * @param frase Es el String usado para la frase
     */
    public void setFrase( String frase ) {
        this.frase = frase;
    }//Cierre del m�todo

}//Cierre de la clase
