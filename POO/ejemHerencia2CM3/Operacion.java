public class Operacion
{
  private int oper1;
  private int oper2;
  private String operacion;
  
  public Operacion( int d1, int d2, String nom )
  {
    this.oper1 = d1;
    this.oper2 = d2;
    this.operacion = nom;
    System.out.println( "Constructor de Operacion.." );
  }
  
  public void setOper1( int dato )
  {
    this.oper1 = dato;
  }
  
  public int getOper1()
  {
    return this.oper1;
  }
  
  public void setOper2( int dato )
  {
    this.oper2 = dato;
  }
  
  public int getOper2()
  {
    return this.oper2;
  }
  
  public void setOperacion( String op )
  {
    this.operacion = op;
  }
  
  public String getOperacion()
  {
    return this.operacion;
  }
  
}