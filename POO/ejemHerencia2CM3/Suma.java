public class Suma extends Operacion2
{
  private int edad;
  
  public Suma( int d1, int d2, int d3, String nom, float d_1 )
  {
    super( d1, d2, d3, nom, d_1);
    System.out.println( "Constructor de Suma.." );
  }
  
  
  public float sumar( int dato1, int dato2, float dato3 )
  {
    setOper1( dato3 );
    setOper2( dato1 );
    setOper3( dato2 );
    
    return obtenerOper1() + getOper2() + getOper3();
  }

  public int sumar( int dato1, int dato2, int dato3 )
  {
    super.setOper1( dato3 );
    super.setOper2( dato1 );
    super.setOper3( dato2 );
    
    return super.getOper1() + super.getOper2() + super.getOper3();
  }
}