public class Operacion2 extends Operacion
{
  private float oper1;
  private int oper3;
  
  public Operacion2( int d1, int d2, int d3, String nom, float d_1 )
  {
    super( d1, d2, nom );
    this.oper1 = d_1;
    this.oper3 = d3;
    System.out.println( "Constructor de Operacion2.." );
  }
  
  public void setOper1( float dato )
  {
    this.oper1 = dato;
  }
  
  public float obtenerOper1()
  {
    return this.oper1;
  }
  
  public void setOper3( int dato )
  {
    this.oper3 = dato;
  }
  
  public int getOper3()
  {
    return this.oper3;
  }
  
}