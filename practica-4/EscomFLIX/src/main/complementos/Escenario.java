package main.complementos;

import main.lib.multimedios.Pelicula;

/**
 *Esta clase se utiliza como el nombre del escenario de cada pelicula que se encuentra en la plataforma EscomFLIX
 * @author osval
 */
public class Escenario {
    //Campo de la clase
    private String EscenarioPelicula;

    /**
     *Constructor para el escenario de la pelicula
     * @param EscenarioPelicula es el escenario principal en el que se lleva a cabo la pelicula
     */
    public Escenario( String EscenarioPelicula ) {
        setEscenarioPelicula( EscenarioPelicula );
    } //Cierre del constructor

    /**
     *Establece los elementos que se usaran dentro del String como nombre del escenario
     * @param EscenarioPelicula es el escenario principal en el que se lleva a cabo la pelicula
     */
    public void setEscenarioPelicula( String EscenarioPelicula ) {
        this.EscenarioPelicula = EscenarioPelicula;
    }//Cierre del m�todo

    /**
     *Devuelve el String establecido como el nombre del escenario
     * @return EscenarioPelicula es el escenario principal en el que se lleva a cabo la pelicula
     */
    public String getEscenarioPelicula() {
        return this.EscenarioPelicula;
    }//Cierre del m�todo
}//Cierre de la clase
