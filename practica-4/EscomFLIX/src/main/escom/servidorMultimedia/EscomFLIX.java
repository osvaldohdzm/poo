﻿package main.escom.servidorMultimedia;

import main.complementos.Escenario;
import java.util.ArrayList;
import java.util.Vector;
import main.lib.personas.Actor;
import main.lib.multimedios.Pelicula;
import main.lib.personas.UsuarioFLIX;

/**
 *En esta clase se declaran los metodos para el uso de la clase Main de la plataforma EscomFLIX
 * @author osval
 */
public class EscomFLIX {
    //Campo de la clase
    /**
     *
     */
    public ArrayList<UsuarioFLIX> GrupoUsuarios = new ArrayList<UsuarioFLIX>();

    /**
     *
     */
    public ArrayList<Pelicula> ListaPeliculas = new ArrayList<Pelicula>();

    /**
     *Constructor de la frase
     *Sirve para declarar las peliculas principales que se  encontraran en la plataforma
     */
    public EscomFLIX() {
        ListaPeliculas.add( new Pelicula( "The Godfather", "Drama", new Escenario( "Italia y la ciudad de estados unidos." ) ) );
        ListaPeliculas.add( new Pelicula( "The Shawshank Redemption", "Drama", new Escenario( "Una prisión." ) ) );
        ListaPeliculas.add( new Pelicula( "The Dark Knight", "Drama", new Escenario( "Ciudad Gótica." ) ) );
        ListaPeliculas.add( new Pelicula( "12 Angry Men", "Drama", new Escenario( "Varios." ) ) );
        ListaPeliculas.add( new Pelicula( "Pulp Fiction", "Drama", new Escenario( "Pueblo estadounidense." ) ) );
    } //Cierre del constructor

    /**
     *Este metodo sirve para agregar un usuario a la plataforma, es el registro; devuelve un valor booleano
     * @param GrupoUsuarios
     * @param usuario
     * @param pass
     * @return
     */
    public boolean AgregarUsuarioFLIX( ArrayList<UsuarioFLIX> GrupoUsuarios, String usuario, String pass ) {
        GrupoUsuarios.add( new UsuarioFLIX( usuario, pass ) );
        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para obtener (y devolver) todos los atributos de algun usuario que se encuentre dentro de la plataforma
     * @param uname
     * @return
     */
    public UsuarioFLIX getUser( String uname ) {
        UsuarioFLIX u;
        if ( GrupoUsuarios != null ) {
            for ( int i = 0 ; i < GrupoUsuarios.size() ; i ++ ) {
                if ( GrupoUsuarios.get( i ).getNombreUsuario() == uname ) {
                    u = GrupoUsuarios.get( i );
                    return u;
                }
            }

        }

        return null;
    }//Cierre del método

    /**
     *Este metodo sirve para que un usuario pueda darse de baja de la plataforma
     * @param GrupoUsuarios
     * @param usuario
     */
    public void BorrarUsuarioFLIX( ArrayList<UsuarioFLIX> GrupoUsuarios, String usuario ) {
        int tam = GrupoUsuarios.size();
        for ( int i = 0 ; i < tam ; i ++ ) {
            if ( ( usuario.equals( GrupoUsuarios.get( i ).getNombreUsuario() ) ) ) {
                GrupoUsuarios.remove( i );
                i = tam;
                System.out.println( "Usuario eliminado" );
            }

        }
    }//Cierre del método

    /**
     *Este metodo sirve para que un usuario pueda pueda modificar su nombre dentro de la plataforma
     * @param GrupoUsuarios
     * @param usuario
     * @param nuevoUsuario
     */
    public void modificarNombreUsuarioFLIX( ArrayList<UsuarioFLIX> GrupoUsuarios, String usuario, String nuevoUsuario ) {
        int tam = GrupoUsuarios.size();
        for ( int i = 0 ; i < tam ; i ++ ) {
            if ( ( usuario.equals( GrupoUsuarios.get( i ).getNombreUsuario() ) ) ) {
                usuario = nuevoUsuario;
                GrupoUsuarios.get( i ).setNombreUsuario( nuevoUsuario );
                i = tam;
                System.out.println( "Nombre de usuario alterado" );
            }

        }
    }//Cierre del método

    /**
     *Este metodo sirve para que un usuario pueda pueda modificar su contraseña dentro de la plataforma
     * @param GrupoUsuarios
     * @param usuario
     * @param pass
     */
    public void modificarPassUsuarioFLIX( ArrayList<UsuarioFLIX> GrupoUsuarios, String usuario, String pass ) {
        int tam = GrupoUsuarios.size();
        for ( int i = 0 ; i < tam ; i ++ ) {
            if ( ( usuario.equals( GrupoUsuarios.get( i ).getNombreUsuario() ) ) ) {
                GrupoUsuarios.get( i ).setContrasena( pass );
                i = tam;
                System.out.println( "Contrasena alterada" );
            }

        }
    }//Cierre del método

    /**
     *Este metodo sirve para verificar que el usuario se haya dado de baja de la plataforma
     * @return
     */
    public boolean BorrarUsuarioFLIX() {
        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para buscar a algun usuario dentro de la plataforma
     * @return
     */
    public boolean BuscarUsuarioFLIX() {
        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para verificar que el usuario se haya modificado su nombre en la plataforma
     * @return
     */
    public boolean modificarUsuarioFLIX() {
        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para poder agregar una pelicula a la plataforma
     * @param p
     * @return
     */
    public boolean AgregarPelicula( Pelicula p ) {
        if ( p != null ) {
            ListaPeliculas.add( p );
        }
        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para poder eliminar una pelicula de la plataforma
     * @param ID
     * @return
     */
    public boolean BorrarPelicula( int ID ) {

        for ( int i = 0 ; i < ListaPeliculas.size() ; i ++ ) {
            if ( ListaPeliculas.indexOf( ListaPeliculas.get( i ) ) == ID ) {
                ListaPeliculas.remove( i );
            }
        }

        return true;
    }//Cierre del método

    /**
     *Este metodo sirve para poder buscar una pelicula dentro de la plataforma
     * @param s
     * @return
     */
    public ArrayList<Pelicula> BuscarPelicula( String s ) {
        ArrayList<Pelicula> a = new ArrayList<Pelicula>();
        for ( int i = 0 ; i < ListaPeliculas.size() ; i ++ ) {
            if ( ListaPeliculas.get( i ).getTituloPelicula().toLowerCase().contains( s.toLowerCase() ) || ListaPeliculas.get( i ).getCategoria().toLowerCase().contains( s.toLowerCase() ) ) {
                a.add( ListaPeliculas.get( i ) );
            }
            else {

            }
        }
        return a;
    }//Cierre del método

    /**
     *
     * @return
     */
    public boolean modificarPelicula() {
        return true;
    }//Cierre del método
}//Cierre de la clase
