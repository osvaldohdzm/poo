/**
* @(#)programa4.java
*
*
* @author Jhanno Fouls Lestrange
* @version 1.00 2007/8/7
*/
/** Siguiendo el ejemplo HolaFecha del capítulo 1, crear un programa
*	"Hola, mundo" que simplemente escriba esa frase. Sólo se necesita un método
*	en la clase (la clase "main" que es la que se ejecuta al
*	correr el programa). Recordar hacerla "static" e incluir la
*	lista de parámetros, incluso aunque no se vayan a usar. Compilar el programa
*	con "javac" y ejecutarlo utilizando "java"
*	Si se utiliza un entorno de desarrollo distinto a JDK , aprender a compilar y
*	ejecutar programas en ese entorno.
*/
public class programa4 {
/** Método principal de la clase */
public static void main(String[] args){
System.out.println("Hola, Mundo...");
System.out.println("Este es el primer ejercicio del Capítulo 1");
}
}
