package utils;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 *
 * @author alumno
 */
public class CSVUtils {

    private static final char DEFAULT_SEPARATOR = ',';

    /**
     *
     * @param w
     * @param values Cadenas que se van leyendo tras cada delimitar coma, con el
     * separador de default que es un espacio
     * @throws IOException
     */
    public static void writeLine( Writer w, List<String> values ) throws IOException {
        writeLine( w, values, DEFAULT_SEPARATOR, ' ' );
    }

    /**
     *
     * @param w
     * @param values Cadenas que se van leyendo tras cada delimitar coma.
     * @param separators Separador de cadenas en el archivo usualmente un
     * espacio
     * @throws IOException
     */
    public static void writeLine( Writer w, List<String> values, char separators ) throws IOException {
        writeLine( w, values, separators, ' ' );
    }

    //https://tools.ietf.org/html/rfc4180
    private static String followCVSformat( String value ) {

        String result = value;
        if ( result.contains( "\"" ) ) {
            result = result.replace( "\"", "\"\"" );
        }
        return result;

    }

    /**
     *
     * @param w
     * @param values
     * @param separators
     * @param customQuote
     * @throws IOException
     */
    public static void writeLine( Writer w, List<String> values, char separators, char customQuote ) throws IOException {

        boolean first = true;

        //default customQuote is empty
        if ( separators == ' ' ) {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuilder sb = new StringBuilder();
        for ( String value : values ) {
            if (  ! first ) {
                sb.append( separators );
            }
            if ( customQuote == ' ' ) {
                sb.append( followCVSformat( value ) );
            }
            else {
                sb.append( customQuote ).append( followCVSformat( value ) ).append( customQuote );
            }

            first = false;
        }
        sb.append( "\n" );
        w.append( sb.toString() );

    }

}
