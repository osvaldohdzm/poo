/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Scanner;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class Validations {

    /**
     *
     * @return
     */
    public static int intInput() {
        Scanner scan = new Scanner( System.in );
        while (  ! scan.hasNextInt() ) {
            System.out.println( "Input is not a number. Try again!" );
            scan.nextLine();
        }
        int number = scan.nextInt();
        return number;
    }

}
