﻿package main;

import main.lib.personas.UsuarioFLIX;
import main.lib.personas.Actor;
import main.complementos.Escenario;
import main.complementos.Guion;
import main.escom.servidorMultimedia.EscomFLIX;
import java.io.IOException;
import java.util.*;
import main.lib.multimedios.Pelicula;
import static utils.Screen.clrscr;
import static utils.Screen.genereateASCII;
import static utils.Validations.intInput;

/**
 *Esta es la clase principal, donde se hara la interaccion directa con el usuario, a la vez que usa metodos y objetos generados de otras clases
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class Main {
    //Campo de la clase
    private static final String APPNAME = "EscomFLIX";
    private static final EscomFLIX escom = new EscomFLIX();
    private static UsuarioFLIX UsuarioActivo;
    private static int choice;
    private static int confirmacion = 0;
    private static final java.io.Console console = System.console();
    private static String contrasenaUsuarioActivo;
    private static String nameUsuarioActivo;
    private static String nuevaContrasena;
    private static String nuevoUsuario;
    private static Scanner scan = new Scanner( System.in );
    private static int tam;

    /**
     * Funcion principal que dirige la ejecución de las pantallas para controlar
     * el flujo del programa.
     * <p>
     * Este método llama al método que contiene el menu prinicpal, luego este
     * menu principal llama los respectivos menus con las opciones de cada
     * selección de usuario.
     *
     * @param args El parámetro args es un array de Strings que debe aparecer
     * obligatoriamente como argumento del método main en un programa Java.
     * @throws java.io.IOException
     */
    public static void main( String[] args ) throws IOException {
        showMainMenu();
    }

    /**
     * Muestra las opciones de un usuario que ha iniciado sesión
     * <p>
     * Este método primero refresca la pantalla, luego muestra un menu con las
     * opciones que tiene el usuario que ha iniciado sesión si la selección es
     * la última termina el programa mediante una llamada al sistema del tipo
     * exit.
     *
     * @param
     * @return
     */
    @SuppressWarnings( "empty-statement" )
    private static void showLoggedUserOptions() throws IOException {
        //UsuarioActivo = escom.GrupoUsuarios.contains( nameUsuarioActivo  );

        do {
            clrscr();
            showBanner();

            System.out.println( "1 Modificar nombre usuario" );
            System.out.println( "2 Modificar contrasena" );
            System.out.println( "3 Borrar usuario" );
            System.out.println( "------------------------" );
            System.out.println( "4 Ver pelicula" );
            System.out.println( "5 Mostrar peliculas" );
            System.out.println( "6 Agregar pelicula" );
            System.out.println( "7 Buscar pelicula" );
            System.out.println( "8 Modificar datos peliculas" );
            System.out.println( "9 Borrar pelicula" );
            System.out.println( "------------------------" );
            System.out.println( "10 Ver preferentes" );
            System.out.println( "11 Anadir a preferentes" );
            System.out.println( "12 Eliminar preferentes" );
            System.out.println( "13 Ver favoritas" );
            System.out.println( "14 Anadir a favoritas" );
            System.out.println( "15 Eliminar favoritas" );
            System.out.println( "16 Cerrar sesion" );
            System.out.println( "\nSeleccion: " );
            choice = intInput();

            switch ( choice ) {
                case 1:
                    clrscr();
                    showBanner();
                    System.out.println( "Nuevo Usuario: " );
                    Scanner escaner3 = new Scanner( System.in );
                    nuevoUsuario = escaner3.nextLine();
                    escom.modificarNombreUsuarioFLIX( escom.GrupoUsuarios, nameUsuarioActivo, nuevoUsuario );
                    nameUsuarioActivo = escom.GrupoUsuarios.get( confirmacion - 1 ).getNombreUsuario();
                    console.readLine( "\n... " );
                    break;
                case 2:
                    clrscr();
                    showBanner();
                    System.out.println( "Nueva Contrasena: " );
                    Scanner escaner4 = new Scanner( System.in );
                    nuevaContrasena = escaner4.nextLine();
                    escom.modificarPassUsuarioFLIX( escom.GrupoUsuarios, nameUsuarioActivo, nuevaContrasena );
                    contrasenaUsuarioActivo = escom.GrupoUsuarios.get( confirmacion - 1 ).getContrasena();
                    console.readLine( "\n... " );
                    break;
                case 3:
                    escom.BorrarUsuarioFLIX( escom.GrupoUsuarios, nameUsuarioActivo );
                    choice = 16;
                    console.readLine( "\n... " );
                    break;
                case 4: {
                    clrscr();
                    showBanner();
                    for ( int i = 0 ; i < escom.ListaPeliculas.size() ; i ++ ) {
                        ArrayList< String> names = new ArrayList< String>();
                        for ( int k = 0 ; k < escom.ListaPeliculas.get( i ).getListaActores().size() ; k ++ ) {
                            names.add( escom.ListaPeliculas.get( i ).getListaActores().get( k ).getNombreActor() );
                        }
                        String allItems = "";
                        allItems = String.join( ", ", names );
                        System.out.println( escom.ListaPeliculas.indexOf( escom.ListaPeliculas.get( i ) ) + ", " + escom.ListaPeliculas.get( i ).getTituloPelicula() + "; " + escom.ListaPeliculas.get( i ).getCategoria() + "; " + escom.ListaPeliculas.get( i ).getEscenario().getEscenarioPelicula() + "; " + allItems );
                    }
                    int id;
                    String strOut = "";
                    String strIn;
                    System.out.print( "\nID: " );
                    id = intInput();

                    if ( escom.ListaPeliculas != null &&  id <= escom.ListaPeliculas.size() - 1 ) {

                        strIn = escom.ListaPeliculas.get( id ).getTituloPelicula();
                        System.out.print( "\nViendo [" + strIn + "]\n" );
                        if ( strIn.length() > 6 ) {
                            strOut = strIn.substring( 0, 7 ) + "...";
                        }
                        genereateASCII( strOut );
                        escom.GrupoUsuarios.get( confirmacion - 1 ).AnadirVistas( escom.ListaPeliculas.get( id ) );
                    }

                    console.readLine( "\n... " );
                    break;
                }
                case 5:
                    clrscr();
                    showBanner();
                    for ( int i = 0 ; i < escom.ListaPeliculas.size() ; i ++ ) {
                        ArrayList< String> names = new ArrayList< String>();
                        for ( int k = 0 ; k < escom.ListaPeliculas.get( i ).getListaActores().size() ; k ++ ) {
                            names.add( escom.ListaPeliculas.get( i ).getListaActores().get( k ).getNombreActor() );
                        }
                        String allItems = "";
                        allItems = String.join( ", ", names );
                        System.out.println( escom.ListaPeliculas.indexOf( escom.ListaPeliculas.get( i ) ) + ", " + escom.ListaPeliculas.get( i ).getTituloPelicula() + "; " + escom.ListaPeliculas.get( i ).getCategoria() + "; " + escom.ListaPeliculas.get( i ).getEscenario().getEscenarioPelicula() + "; " + allItems );
                    }
                    console.readLine( "\n... " );
                    break;
                case 6: {
                    clrscr();
                    showBanner();
                    String n = console.readLine( "\nTitulo: " );
                    String c = console.readLine( "\nCategoria: " );
                    String e = console.readLine( "\nEscenario: " );
                    String la = console.readLine( "\nLista de actores (separados por coma): " );
                    ArrayList< String> aList = new ArrayList( Arrays.asList( la.split( "," ) ) );
                    ArrayList< Actor> actorList = new ArrayList< Actor>();
                    for ( int i = 0 ; i < aList.size() ; i ++ ) {
                        actorList.add( new Actor( new Guion( "Seduce mucho mejor una insinuación que una aparatosa muestra gratuita del físico" ), "La bella y la bestia, El círculo, Colonia Dignidad", aList.get( i ) ) );
                    }
                    escom.ListaPeliculas.add( new Pelicula( actorList, n, c, new Escenario( e ) ) );
                    break;
                }
                case 7:
                    clrscr();
                    showBanner();
                    String m = console.readLine( "\nNombre: " );
                    ArrayList< Pelicula> listapelicula = new ArrayList<>();
                    listapelicula = escom.BuscarPelicula( m );
                    for ( int i = 0 ; i < listapelicula.size() ; i ++ ) {
                        System.out.println( escom.ListaPeliculas.indexOf( listapelicula.get( i ) ) + ", " + listapelicula.get( i ).getTituloPelicula() + "; " + listapelicula.get( i ).getCategoria() + "; " + listapelicula.get( i ).getEscenario().getEscenarioPelicula() );
                    }
                    console.readLine( "\n... " );
                    break;
                case 8: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    
                    if ( escom.ListaPeliculas != null &&  id <= escom.ListaPeliculas.size() - 1 ) {
                         escom.ListaPeliculas.get( id ).setTituloPelicula( console.readLine( "\nTitulo: " ) );
                    escom.ListaPeliculas.get( id ).setCategoria( console.readLine( "\nCategoria: " ) );
                    escom.ListaPeliculas.get( id ).getEscenario().setEscenarioPelicula( console.readLine( "\nEscenario: " ) );
                    String la = console.readLine( "\nLista de actores (separados por coma): " );
                    ArrayList< String> aList = new ArrayList( Arrays.asList( la.split( "," ) ) );
                    ArrayList< Actor> actorList = new ArrayList< Actor>();
                    for ( int i = 0 ; i < aList.size() ; i ++ ) {
                        actorList.add( new Actor( new Guion( "Seduce mucho mejor una insinuación que una aparatosa muestra gratuita del físico" ), "La bella y la bestia, El círculo, Colonia Dignidad", aList.get( i ) ) );
                    }
                    escom.ListaPeliculas.get( id ).setListaActores( actorList );
                    }
                    
                   
                    console.readLine( "\n... " );
                    break;
                }
                case 9: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    
                    if ( escom.ListaPeliculas != null && id <= escom.ListaPeliculas.size() - 1 ) {
                     escom.BorrarPelicula( id );
                    }
                    
                   
                    console.readLine( "\n... " );
                    break;
                }
                case 10: {
                    clrscr();
                    showBanner();
                    Stack< Pelicula> p = new Stack<>();
                    p = escom.GrupoUsuarios.get( confirmacion - 1 ).getPreferences();
                    for ( int i = 0 ; i < p.size() ; i ++ ) {

                        System.out.println( p.get( i ).getTituloPelicula() ); // extrae un elemento de la pila

                    }
                    console.readLine( "\n... " );
                    break;
                }
                case 11: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    escom.GrupoUsuarios.get( confirmacion - 1 ).AnadirPreferentes( escom.ListaPeliculas.get( id ) );
                    console.readLine( "\n... " );
                    break;
                }
                case 12: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    escom.GrupoUsuarios.get( confirmacion - 1 ).EliminarPreferentes( id );
                    console.readLine( "\n... " );
                    break;
                }
                case 13: {
                    clrscr();
                    showBanner();
                    Stack< Pelicula> p = new Stack<>();
                    p = escom.GrupoUsuarios.get( confirmacion - 1 ).getFavoritos();
                    for ( int i = 0 ; i < p.size() ; i ++ ) {
                        System.out.println( p.get( i ).getTituloPelicula() ); // extrae un elemento de la pila
                    }
                    console.readLine( "\n... " );
                    break;
                }
                case 14: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    escom.GrupoUsuarios.get( confirmacion - 1 ).AnadirFavoritos( escom.ListaPeliculas.get( id ) );
                    console.readLine( "\n... " );
                    break;
                }
                case 15: {
                    clrscr();
                    showBanner();
                    int id;
                    System.out.print( "\nID: " );
                    id = intInput();
                    escom.GrupoUsuarios.get( confirmacion - 1 ).EliminarFavorito( id );
                    console.readLine( "\n... " );
                    break;
                }
                default:
                    break;
            }
        }
        while ( choice != 16 );
    }

    /**
     * Muestra las opciones de un usuario para dirigir el flujo programa.
     * <p>
     * Este método primero refresca la pantalla, luego muestra una serie de
     * opciones para el usuario, en este caso para poder iniciar sesión y
     * realizar operacciones permitidas según el usuario y su contenido
     * asociado. Si la selección es la última termina el programa mediante una
     * llamada al sistema del tipo exit.
     *
     * @param
     * @return
     */
    private static void showMainMenu() throws IOException {
        do {
            clrscr();

            showBanner();
            System.out.println( "\n" + APPNAME );
            System.out.println( "1 Iniciar sesion" );
            System.out.println( "2 Registro" );
            System.out.println( "3 Salir" );
            System.out.println( "\nSeleccion: " );
            choice = intInput();

            confirmacion = 0;

            if ( choice == 1 ) {
                clrscr();
                showBanner();

                nameUsuarioActivo = console.readLine( "Username: " );
                contrasenaUsuarioActivo = new String( console.readPassword( "Password: " ) );

                tam = escom.GrupoUsuarios.size();
                if ( tam == 0 ) {
                    System.out.println( "Usuario y/o contrasena incorrecto(s)!" );

                }
                for ( int i = 0 ; i < tam ; i ++ ) {
                    if ( ( nameUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getNombreUsuario() ) ) && ( contrasenaUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getContrasena() ) ) ) {
                        confirmacion = i + 1;
                        i = tam;
                        System.out.println( "Inicio de sesion correcto!" );
                    }
                    else if ( i == ( tam - 1 ) && ( (  ! nameUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getNombreUsuario() ) ) || (  ! contrasenaUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getContrasena() ) ) ) ) {
                        System.out.println( "Usuario y/o contrasena incorrecto(s)!" );

                    }

                }
                console.readLine( "\n... " );
                if ( confirmacion != 0 ) {
                    showLoggedUserOptions();
                }
            }
            else if ( choice == 2 ) {
                clrscr();
                showBanner();

                nameUsuarioActivo = console.readLine( "Username: " );
                contrasenaUsuarioActivo = new String( console.readPassword( "Password: " ) );
                tam = escom.GrupoUsuarios.size();
                if ( tam == 0 ) {
                    System.out.println( "Registro realizado correctamente!" );
                    escom.AgregarUsuarioFLIX( escom.GrupoUsuarios, nameUsuarioActivo, contrasenaUsuarioActivo );
                }
                for ( int i = 0 ; i < tam ; i ++ ) {
                    if ( nameUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getNombreUsuario() ) ) {
                        i = tam;
                        System.out.println( "El nombre de usuario ya existe!" );

                    }
                    if ( i == ( tam - 1 ) && (  ! nameUsuarioActivo.equals( escom.GrupoUsuarios.get( i ).getNombreUsuario() ) ) ) {
                        System.out.println( "Registro realizado correctamente!" );
                        escom.AgregarUsuarioFLIX( escom.GrupoUsuarios, nameUsuarioActivo, contrasenaUsuarioActivo );
                    }

                }
                console.readLine( "\n... " );
            }
        }
        while ( choice != 3 );
        System.exit( 0 );
    }

    /**
     * Muestra un banner ASCII con el título del programa.
     * <p>
     * Este método muestra un banner con el título de la aplicación este es
     * igual al APPNAME.
     *
     * @param
     * @return
     */
    private static void showBanner() {
        System.out.println( "\n███████╗███████╗.██████╗.██████╗.███╗...███╗███████╗██╗.....██╗██╗..██╗\n██╔════╝██╔════╝██╔════╝██╔═══██╗████╗.████║██╔════╝██║.....██║╚██╗██╔╝\n█████╗..███████╗██║.....██║...██║██╔████╔██║█████╗..██║.....██║.╚███╔╝.\n██╔══╝..╚════██║██║.....██║...██║██║╚██╔╝██║██╔══╝..██║.....██║.██╔██╗.\n███████╗███████║╚██████╗╚██████╔╝██║.╚═╝.██║██║.....███████╗██║██╔╝.██╗\n╚══════╝╚══════╝.╚═════╝.╚═════╝.╚═╝.....╚═╝╚═╝.....╚══════╝╚═╝╚═╝..╚═╝\n.......................................................................\n\n" );
    }

}
