package main.lib.multimedios;

import java.util.ArrayList;
import java.util.Vector;
import main.escom.servidorMultimedia.EscomFLIX;
import main.lib.personas.UsuarioFLIX;
import main.lib.personas.Actor;
import main.complementos.Escenario;

/**
 *Esta clase se utiliza para establecer los atributos que tendr�n las peliculas en la plataforma de EscomFLIX
 * @author osval
 */
public class Pelicula {
    //Campos de la clase
    private ArrayList<Actor> listaActores;
    private String tituloPelicula;
    private String categoria;
    private Escenario escenario;
    private int idPelicula;

    /**
     *Constructor para las peliculas
     * @param tituloPelicula Define el titulo usando String
     * @param categoria Define la categoria usando String
     * @param escenario Define el escenario usando String
     */
    public Pelicula( String tituloPelicula, String categoria, Escenario escenario ) {
        this.listaActores = new ArrayList<Actor>();
        this.tituloPelicula = tituloPelicula;
        this.categoria = categoria;
        this.escenario = escenario;
    } //Cierre del constructor

    /**
     *Constructor para las peliculas
     * @param listaActores Define un arreglo de objetos para la lista de actores
     * @param tituloPelicula Define el titulo usando String
     * @param categoriaDefine la categoria usando String
     * @param escenario Define el escenario usando String
     */
    public Pelicula( ArrayList<Actor> listaActores, String tituloPelicula, String categoria, Escenario escenario ) {
        this.listaActores = listaActores;
        this.tituloPelicula = tituloPelicula;
        this.categoria = categoria;
        this.escenario = escenario;
    } //Cierre del constructor

    /**
     *Constructor para las peliculas
     * @param listaActores Define un arreglo de objetos para la lista de actores
     * @param tituloPelicula Define el titulo usando String
     * @param categoriaDefine la categoria usando String
     * @param escenario Define el escenario usando String
     * @param idPelicula Define el ID de la pelicula usando un entero
     */
    public Pelicula( ArrayList<Actor> listaActores, String tituloPelicula, String categoria, Escenario escenario, int idPelicula ) {
        this.listaActores = listaActores;
        this.tituloPelicula = tituloPelicula;
        this.categoria = categoria;
        this.escenario = escenario;
        this.idPelicula = idPelicula;
    } //Cierre del constructor

    /**
     *Metodo que devuelve la lista de actores que actuan en la pelicula
     * @returnlistaActores Define un arreglo de objetos para la lista de actores
     */
    public ArrayList<Actor> getListaActores() {
        return this.listaActores;
    }//Cierre del m�todo

    /**
     *Metodo que establece la lista de actores que actuan en la pelicula
     * @param listaActores Define un arreglo de objetos para la lista de actores
     */
    public void setListaActores( ArrayList<Actor> listaActores ) {
        this.listaActores = listaActores;
    }//Cierre del m�todo

    /**
     *Metodo que devuelve el titulo de la pelicula
     * @return tituloPelicula Define el titulo usando String
     */
    public String getTituloPelicula() {
        return this.tituloPelicula;
    }//Cierre del m�todo

    /**
     *Metodo que establece el titulo de la pelicula
     * @param tituloPelicula Define el titulo usando String
     */
    public void setTituloPelicula( String tituloPelicula ) {
        this.tituloPelicula = tituloPelicula;
    }//Cierre del m�todo

    /**
     *Metodo que devuelve la categoria de la pelicula
     * @return categoria Define la categoria usando String
     */
    public String getCategoria() {
        return this.categoria;
    }//Cierre del m�todo

    /**
     *Metodo que establece la categoria de la pelicula
     * @param categoria Define la categoria usando String
     */
    public void setCategoria( String categoria ) {
        this.categoria = categoria;
    }//Cierre del m�todo

    /**
     *Metodo que devuelve el escenario de la pelicula
     * @return escenario Define el escenario usando String
     */
    public Escenario getEscenario() {
        return this.escenario;
    }//Cierre del m�todo

    /**
     *Metodo que establece el escenario de la pelicula
     * @param escenario Define el escenario usando String
     */
    public void setEscenario( Escenario escenario ) {
        this.escenario = escenario;
    }//Cierre del m�todo

    /**
     *Metodo que devuelve el ID de la pelicula
     * @return idPelicula Define el ID de la pelicula usando un entero
     */
    public int getIdPelicula() {
        return idPelicula;
    }//Cierre del m�todo

    /**
     *Metodo que establece el ID de la pelicula
     * @param idPelicula Define el ID de la pelicula usando un entero
     */
    public void setIdPelicula( int idPelicula ) {
        this.idPelicula = idPelicula;
    }//Cierre del m�todo

}//Cierre de la clase
