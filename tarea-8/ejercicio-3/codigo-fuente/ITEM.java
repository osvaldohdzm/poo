package main;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class ITEM {

    private int idProd;
    private int cantidad;

    /**
     *
     * @return
     */
    public int getIdProd() {
        return idProd;
    }

    /**
     *
     * @param idProd
     */
    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    /**
     *
     * @return
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @param idProde ID del producto referencia al ID del producto en almacen.
     * @param cantidade Cantidad de productos con una ID específica en el carrito.
     */
    public ITEM(int idProde, int cantidade) {
        this.idProd = idProde;
        this.cantidad = cantidade;
    }

}
