/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class Screen {

    /**
     * Refresca el contenido de la pantalla.
     * <p>
     * Este método primero intenta verificar si en el objeto sistema la propidad
     * name contiene la palabra Windows y así utilizar el comando cls para la la
     * limpieza o refrescado de la pantalla. En caso de que el sistema no sea
     * windows o que la propiedad name no sea identica a ese indentificador de
     * etiqueta entonces se utiliza el comando clear propio de sistemas UNIX o
     * basados en UNIX como GNU/Linux. En caso de no tener exito con el intento
     * se lanza una interrupción del programa.
     *
     */
    public static void clrscr() {
        //Clears Screen in java
        try {
            if ( System.getProperty( "os.name" ).contains( "Windows" ) ) {
                new ProcessBuilder( "cmd", "/c", "cls" ).inheritIO().start().waitFor();

            }
            else {
                Runtime.getRuntime().exec( "clear" );

            }
        }
        catch ( IOException | InterruptedException ex ) {
        }
    }

    /**
     *
     * @param s
     * @throws IOException
     */
    public static void genereateASCII( String s ) throws IOException {
        BufferedImage image = new BufferedImage( 144, 32, BufferedImage.TYPE_INT_RGB );
        Graphics g = image.getGraphics();
        g.setFont( new Font( "Dialog", Font.PLAIN, 24 ) );
        Graphics2D graphics = ( Graphics2D ) g;
        graphics.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING,
                                   RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
        graphics.drawString( s, 6, 24 );
        ImageIO.write( image, "png", new File( "text.png" ) );

        for ( int y = 0 ; y < 32 ; y ++ ) {
            StringBuilder sb = new StringBuilder();
            for ( int x = 0 ; x < 144 ; x ++ ) {
                sb.append( image.getRGB( x, y ) == -16777216 ? " " : image.getRGB( x, y ) == -1 ? "#" : "*" );
            }
            if ( sb.toString().trim().isEmpty() ) {
                continue;
            }
            System.out.println( sb );
        }
    }

}
