/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import main.complementos.Escenario;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import main.lib.multimedios.Pelicula;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class CSVManage {

    static String csvFilePath;
    static ArrayList<Pelicula> moviesDataList;

    /**
     * <p>
     * Lee los datos de un archivo CSV y los carga en una lista.
     *
     * @throws IOException
     */
    public static void createDefaultCSV() throws IOException {
        String csvFilePath = new File( "data.csv" ).getAbsolutePath();
        FileWriter writer = new FileWriter( csvFilePath );

        moviesDataList = new ArrayList<Pelicula>();
        moviesDataList.add( new Pelicula( "Avengers 4", "Heroes", new Escenario( " \"Tierra, espacio.\"" ) ) );

        for ( int i = 0 ; i < moviesDataList.size() ; i ++ ) {
            //     
            CSVUtils.writeLine( writer, Arrays.asList( moviesDataList.get( i ).getTituloPelicula(), moviesDataList.get( i ).getTituloPelicula() ) );
        }
        writer.flush();
        writer.close();
    }

    /**
     * <p>
     * Lee los datos de un archivo CSV y los carga en una lista.
     *
     * @param csvFilePath Archivo CSV a leer.
     * @return Una lista de arrays string con las líneas del archivo CSV.
     * @throws IOException
     */
    public static List<String[]> readData( String csvFilePath ) throws IOException {
        File f = new File( csvFilePath );
        List<String[]> content = new ArrayList<String[]>();
        try ( BufferedReader br = new BufferedReader( new FileReader( f ) ) ) {
            String line = "";
            while ( ( line = br.readLine() ) != null ) {
                content.add( line.split( "," ) );
            }
        }
        catch ( FileNotFoundException e ) {
            //Some error logging
        }
        return content;
    }

    /**
     * <p>
     * Lee los datos de un archivo CSV y lo imprime en pantalla con un formato
     * establecido.
     *
     * @param csvFilePath Archivo CSV a leer.
     * @throws IOException
     */
    public static void showCSV( String csvFilePath ) throws IOException {
        List<String[]> datos = readData( csvFilePath );

        String format = "%-15s%-15s%-65s%-18s%-15s%-15s%n";

        System.out.printf( "\n\n/******************************************************************************/\n" );
        for ( String[] strings : datos ) {
            System.out.printf( format, strings[ 0 ], strings[ 1 ], strings[ 2 ], strings[ 3 ], strings[ 4 ], strings[ 5 ] );
        }

        System.out.printf( "\n/******************************************************************************/\n\n" );
    }

}
