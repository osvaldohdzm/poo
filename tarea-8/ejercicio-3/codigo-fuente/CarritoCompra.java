package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class CarritoCompra {

    private ArrayList<ITEM> items;
    private int ID;

    CarritoCompra() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * <p> 
     * Obtiene los items de un carrito. Ver también. {@link #CarritoCompra}.
     * @return  
     */
    public ArrayList<ITEM> getItems() {
        return items;
    }

    /**
     * <p> 
     * Consutrctor por default
     * @param items
     * @param tempID
     */
    public CarritoCompra(ArrayList<ITEM> items, int tempID) {
        this.items = items;
        this.ID = tempID;
    }

    /**
     *
     * @param items
     */
    public void setItems(ArrayList<ITEM> items) {
        this.items = items;
    }

    /**
     * <p> 
     * Agrega productos al carrito.
     * @param idProd
     * @param cant
     */
    public void agregarProducto(int idProd, int cant) {
        Boolean exist = false;

        for (int i = 0; i < this.getItems().size(); i++) {
            if (items.get(i).getIdProd() == idProd) {
                items.get(i).setCantidad(items.get(i).getCantidad() + cant);
                exist = true;
            }
        }
        if (!exist) {
            ITEM itemNew = new ITEM(idProd, cant);
            items.add(itemNew);
        }
    }

    /**
     * <p> 
     * Borra un producto usando su id siempre y cuando exista en el carrito.
     * @param idProd
     * @return
     */
    public ITEM borrarProducto(int idProd) {
        ITEM item = null;

        for (int i = 0; i < this.getItems().size(); i++) {
            if (items.get(i).getIdProd() == idProd) {
                item = items.get(i);
                items.remove(i);
            }
        }

        return item;
    }

    /**
     *
     * @return @throws java.io.IOException
     */
    public double calcularTotal() throws IOException {
        double total = 0;
        float semitotal = 0;
        ArrayList<Producto> stockList = Main.convertCSVtoStockList(Main.csvFile);
        for (int i = 0; i < items.size(); i++) {
            if (stockList.get(i).getId() == items.get(i).getIdProd()) {
                semitotal = items.get(i).getCantidad() * stockList.get(i).getPrecio();
                total = total + semitotal;
            }
        }
        return total;
    }

    /**
     *
     * @return
     */
    public boolean vaciarCarrito() {
        this.items.clear();
        return true;
    }

    /**
     *
     */
    public void checkOut() {
        System.out.printf("\n\n/******************************************************************************/\n");
        String description = new String();
        String format = "%-15s%-15s%n";
        for (int i = 0; i < this.getItems().size(); i++) {
            System.out.printf(format, "Producto:", description);
            System.out.printf(format, "Cantidad:", items.get(i).getCantidad());
            System.out.printf("\n- - - - - - - - - - - - \n");
        }
        System.out.printf("\n/******************************************************************************/\n\n");
    }

    /**
     * <p> 
     * Recibe la lista del almacen, compara los items del carrito con la existencia en almacen y calcula el total.
     * @param stockList
     * @throws Exception
     */
    public void checkOut(ArrayList<Producto> stockList) throws Exception {
        String format = "%-15s%-15s%n";
        try {
            float total = 0;
            System.out.printf("\n\n/******************************************************************************/\n");
            System.out.printf("Carrito:" + this.ID + "\n\n");
            for (int i = 0; i < items.size(); i++) {
                if (stockList.get(i).getId() == items.get(i).getIdProd()) {
                    System.out.printf(format, "ID:", items.get(i).getIdProd());
                    System.out.printf(format, "Producto:", stockList.get(i).getDescripcion());
                    System.out.printf(format, "Cantidad:", items.get(i).getCantidad());
                }
            }

            System.out.printf("\nTotal: \t $" + calcularTotal() + "\n");

            System.out.printf("\n/******************************************************************************/\n\n");
        } catch (IndexOutOfBoundsException ioob) {
            String[] args = {Main.csvFile};
            Main.main(args);
        }

    }

}
