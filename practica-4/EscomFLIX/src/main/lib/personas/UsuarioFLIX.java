package main.lib.personas;

import java.util.Stack;
import main.escom.servidorMultimedia.EscomFLIX;
import main.lib.multimedios.Pelicula;

/**
 *Esta clase sirve para establecer los atributos que tendrá 
 * el usuario al momento de accesar a la plataforma EscomFLIX
 * @author osval
 */
public class UsuarioFLIX {
    //Campo de la clase
    private Stack<Pelicula> buscadas = new Stack<Pelicula>();
    private EscomFLIX content;
    private String contrasena;
    private Stack<Pelicula> favoritos = new Stack<Pelicula>();
    private Stack<Pelicula> historialVistas = new Stack<Pelicula>();
    ;
    private String nombreUsuario;
    private Stack<Pelicula> preferences = new Stack<Pelicula>();
    private String tipoUsuario;

    /**
     *Constructor del usuario de la plataforma
     * @param nombreUsuario nombre con el que el usuario ingresa a la plataforma
     * @param contrasena contrase�a con la que el usuario ingresa a la plataforma
     * @param tipoUsuario
     * @param favoritos lista (pila) de peliculas del usuario
     * @param preferences lista (pila) de peliculas del usuario
     * @param buscadas historial de peliculas que ha buscado el usuario
     * @param historialVistas historial de peliculas que ha visto el usuario
     */
    public UsuarioFLIX( String nombreUsuario, String contrasena, String tipoUsuario, Stack<Pelicula> favoritos, Stack<Pelicula> preferences, Stack<Pelicula> buscadas, Stack<Pelicula> historialVistas ) {
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        this.tipoUsuario = tipoUsuario;
        this.favoritos = favoritos;
        this.preferences = preferences;
        this.buscadas = buscadas;
        this.historialVistas = historialVistas;
        content = new EscomFLIX();

    } //Cierre del constructor

    /**
     *Constructor del usuario de la plataforma
     * @param nombreUsuario
     * @param contrasena
     */
    public UsuarioFLIX( String nombreUsuario, String contrasena ) {
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        content = new EscomFLIX();
        this.favoritos = new Stack<Pelicula>();
        this.preferences = new Stack<Pelicula>();
        this.buscadas = new Stack<Pelicula>();
    } //Cierre del constructor

    /**
     *Este metodo agrega una pelicula a la lista favoritos y devuelve un valor booleano
     * @param p
     * @return
     */
    public boolean AnadirFavoritos( Pelicula p ) {
        favoritos.push( p );
        System.out.println( "agregada la pelicula" );

        return true;
    }

    /**
     *Este metodo agrega una pelicula a la lista preferentes y devuelve un valor booleano
     * @param p
     * @return
     */
    public boolean AnadirPreferentes( Pelicula p ) {
        preferences.push( p );

        return true;
    }//Cierre del m�todo

    /**
     *Este metodo sirve para mostrar el historial de peliculas vistas y devuelve un valor booleano
     * @param p
     * @return
     */
    public boolean AnadirVistas( Pelicula p ) {
        historialVistas.add( p );
        return true;

    }//Cierre del m�todo

    /**
     *Este metodo sirve para mostrar el historial de peliculas buscadas y devuelve un valor booleano
     * @param idPreferentes
     * @return
     */
    public boolean ConsultarBuscadas( int idPreferentes ) {
        while (  ! buscadas.isEmpty() ) { // mostrar pila completa
            System.out.println( buscadas.pop().getIdPelicula() ); // extrae un elemento de la pila
        }
        System.out.println( "todas las peliculas de buscadas" );
        return true;

    }//Cierre del m�todo

    /**
     *Este metodo sirve para consultar la lista de peliculas favoritas y devuelve un valor booleano
     * @return
     */
    public boolean ConsultarFavoritos() {
        while (  ! favoritos.isEmpty() ) { // mostrar pila completa
            System.out.println( favoritos.pop().getIdPelicula() ); // extrae un elemento de la pila
        }
        System.out.println( "todas las peliculas de favoritos" );
        return true;
    }//Cierre del m�todo

    /**
     *Este metodo sirve para consultar la lista de peliculas preferentes
     * @param preferences
     * @return
     */
    public Stack<Pelicula> ConsultarPreferentes( Stack<Pelicula> preferences ) {
        Stack<Pelicula> copy = new Stack<Pelicula>();
        copy = preferences;
        return copy;
    }//Cierre del m�todo

    /**
     *Este metodo sirve para mostrar el historial de peliculas vistas y devuelve un valor booleano
     * @return
     */
    public boolean ConsultarVistas() {
        while (  ! historialVistas.isEmpty() ) { // mostrar pila completa
            System.out.println( historialVistas.pop().getIdPelicula() ); // extrae un elemento de la pila
        }
        System.out.println( "todas las peliculas vistas" );
        return true;

    }//Cierre del m�todo

    /**
     *Este metodo sirve para eliminar una pelicula de la lista favoritos
     * @param idPelicula
     * @return
     */
    public boolean EliminarFavorito( int idPelicula ) {
        System.out.println( "elimino la pelicula" + idPelicula );
        favoritos.remove( idPelicula );
        return true;
    }//Cierre del m�todo

    /**
     *Este metodo sirve para eliminar una pelicula de la lista Preferentes
     * @param idPelicula
     * @return
     */
    public boolean EliminarPreferentes( int idPelicula ) {
        preferences.remove( idPelicula );
        System.out.println( "elimino la pelicula" + idPelicula );
        return true;
    }//Cierre del m�todo

    /**
     *Este metodo devuelve la pila de las peliculas buscadas
     * @return
     */
    public Stack<Pelicula> getBuscadas() {
        return buscadas;
    }//Cierre del m�todo

    /**
     *Este metodo establece la pila de las peliculas buscadas
     * @param buscadas
     */
    public void setBuscadas( Stack<Pelicula> buscadas ) {
        this.buscadas = buscadas;
    }

    /**
     *Este metodo devuelve la contrase�a del usuario de la plataforma
     * @return
     */
    public String getContrasena() {
        return contrasena;
    }//Cierre del m�todo

    /**
     *Este metodo establece la contrase�a del usuario de la plataforma
     * @param contrasena
     */
    public void setContrasena( String contrasena ) {
        this.contrasena = contrasena;
    }//Cierre del m�todo

    /**
     Este metodo devuelve la pila de la lista Favoritos
     * @return
     */
    public Stack<Pelicula> getFavoritos() {
        return favoritos;
    }//Cierre del m�todo

    /**
     *Este metodo establece la pila de la lista Favoritos
     * @param favoritos
     */
    public void setFavoritos( Stack<Pelicula> favoritos ) {
        this.favoritos = favoritos;
    }//Cierre del m�todo

    /**
     *Este metodo devuelve la pila del historial de peliculas vistas
     * @return
     */
    public Stack<Pelicula> getHistorialVistas() {
        return historialVistas;
    }//Cierre del m�todo

    /**
     *Este metodo establece la pila del historial de peliculas vistas
     * @param historialVistas
     */
    public void setHistorialVistas( Stack<Pelicula> historialVistas ) {
        this.historialVistas = historialVistas;
    }//Cierre del m�todo

    /**
     *Este metodo devuelve el nombre de usuario de la plataforma
     * @return
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }//Cierre del m�todo

    /**
     *Este metodo establece el nombre de usuario de la plataforma
     * @param nombreUsuario
     */
    public void setNombreUsuario( String nombreUsuario ) {
        this.nombreUsuario = nombreUsuario;
    }//Cierre del m�todo

    /**
     *Este metodo devuelve la pila de la lista Preferentes
     * @return
     */
    public Stack<Pelicula> getPreferences() {
        return preferences;
    }//Cierre del m�todo

    /**
     *Este metodo establece la pila de la lista Preferentes
     * @param preferences
     */
    public void setPreferences( Stack<Pelicula> preferences ) {
        this.preferences = preferences;
    }//Cierre del m�todo

    /**
     *Este metodo devuelve el valor de tipo Usuario
     * @return
     */
    public String getTipoUsuario() {
        return tipoUsuario;
    }//Cierre del m�todo

    /**
     *Este metodo establece el valor de tipo Usuario
     * @param tipoUsuario
     */
    public void setTipoUsuario( String tipoUsuario ) {
        this.tipoUsuario = tipoUsuario;
    }//Cierre del m�todo

}//Cierre de la clase
