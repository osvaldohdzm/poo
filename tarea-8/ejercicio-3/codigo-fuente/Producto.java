package main;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN) do
 * Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class Producto {

    private int id;
    private float precio;
    private String descripcion;
    private String marca;
    private int cantidad;
    private String departamento;

    /**
     *
     * @return Regresa la ID del producto.
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id Asigna un valor al id del producto.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return Regresa el precio del producto.
     */
    public float getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio Asigna el precio al id del producto.
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    /**
     *
     * @return Regresa la descripción del producto.
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *
     * @param descripcion Asigna la descripción del producto.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     *
     * @return Regresa la marca del producto.
     */
    public String getMarca() {
        return marca;
    }

    /**
     *
     * @param marca Asigna la marca del producto.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * 
     * @return  La cantidad del producto en el carrito.
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     *
     * @param cantidad La cantidad del producto en el carrito.
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @return El departamento del producto en el carrito.
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     *
     * @param departamento El departamento del producto en el carrito.
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * <p>
     * Constructor para producto vacio.
     */
    public Producto() {
    }

    /**
     * <p>
     * Constructor para producto por default.
     * @param id
     * @param precio
     * @param descripcion
     * @param marca
     * @param cantidad
     * @param departamento
     */
    public Producto(int id, float precio, String descripcion, String marca, int cantidad, String departamento) {
        this.id = id;
        this.precio = precio;
        this.descripcion = descripcion;
        this.marca = marca;
        this.cantidad = cantidad;
        this.departamento = departamento;
    }

}
