package main;

import com.csv.utils.CSVUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import static main.Main.readData;

/**
 *
 * @author Osvaldo Hernandez Morales at Escuela Superior de Cómputo (IPN)
 */
public class Main {

    static ArrayList<Producto> stockList;
    static String csvFile;
    static ArrayList<CarritoCompra> cartList;

    /**
     * <p>
     * Este método muestra el menú del programa.
     * @param args Argumentos de la función principal que se recibe en consola en este caso el archivo CSV con los productos del almacen.
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        csvFile = args[0];
        fileValidation(csvFile);

        Scanner scan = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);
        stockList = convertCSVtoStockList(csvFile);
        cartList = loadCarts();
        int choice = 0;
        int input = 0;
        String input1;
        int carSelection = 0;

        do {
            System.out.println("\nMenu - Shopping Cart Options");
            System.out.println("1 Seleccionar carrito (default: 1)");
            System.out.println("2 Agregar producto");
            System.out.println("3 Quitar producto");
            System.out.println("4 Ver productos en stock");
            System.out.println("5 Checkout de carrito");
            System.out.println("6 Vaciar carrito");
            System.out.println("7 Salir");
            System.out.println("\nSeleccion: ");
            choice = scan.nextInt();

            if (choice == 1) {
                System.out.println("\nCarritos actuales:");
                showCarts(cartList);
                System.out.println("Introduce el numero de carrito:");
                input1 = scanner.nextLine();
                if (isNumeric(input1)) {
                    carSelection = Integer.parseInt(input1) - 1;
                } else {
                    System.out.println("\n...Introduce un numero!");
                    break;
                }
            } else if (choice == 2) {
                int id = 0;
                int cant = 0;
                System.out.println("ID del producto:");
                input1 = scanner.nextLine();
                if (isNumeric(input1)) {
                    id = Integer.parseInt(input1);
                } else {
                    System.out.println("\n...Introduce un numero!");
                    break;
                }
                System.out.println("Cantidad:");
                input1 = scanner.nextLine();
                if (isNumeric(input1)) {
                    cant = Integer.parseInt(input1);
                } else {
                    System.out.println("\n...Introduce un numero!");
                    break;
                }
                cartList.get(carSelection).agregarProducto(id, cant);
            } else if (choice == 3) {
                System.out.println("Escribe el ID del producto a eliminar:");
                input = scan.nextInt();
                System.out.println(cartList.get(carSelection).borrarProducto(input).getIdProd() + " eliminado");
            } else if (choice == 4) {
                showProducts(stockList);
            } else if (choice == 5) {
                cartList.get(carSelection).checkOut(stockList);
            } else if (choice == 6) {
                cartList.get(carSelection).vaciarCarrito();
            }
            System.out.println("\nSelección: ");
        } while (choice != 7);
        saveCarts();
        System.exit(0);
    }

    private static void showCarts(ArrayList<CarritoCompra> cartList) {
        System.out.printf("\n\n/******************************************************************************/\n");
        for (int i = 1; i < cartList.size() + 1; i++) {
            System.out.printf("Carrito " + i + "\n");
        }
        System.out.printf("\n/******************************************************************************/\n\n");
    }

    /**
     * <p>
     * Verifica que la cadena que esta como parametro de entrada sea número de vuelve true en caso que así sea.
     * @param maybeNumeric Cadena a verificar.
     * @return Verdadero o falso en caso de que la cadena sea un número.
     */
    public static boolean isNumeric(String maybeNumeric) {
        boolean state;
        if (maybeNumeric != null && maybeNumeric.matches("[0-9]+")) {
            state = true;
        } else {
            state = false;

        }
        return state;
    }

    /**
     * <p>
     * Muestra los productos en el almacen cargado al programa y los imprime en pantalla.
     * @param stockList Lista de almacen cargada en el programa.
     * @return 0 en caso de exito.
     */
    private static int showProducts(ArrayList<Producto> stockList) {
        String format = "%-15s%-15s%n";
        System.out.printf("\n\n/******************************************************************************/\n");

        for (int i = 0; i < stockList.size(); i++) {
            System.out.printf(format, "ID:", stockList.get(i).getId());
            System.out.printf(format, "Descripción:", stockList.get(i).getDescripcion());
            System.out.printf(format, "Cantidad:", stockList.get(i).getCantidad());
            System.out.printf(format, "Precio:", stockList.get(i).getPrecio());
            System.out.printf(format, "Marca:", stockList.get(i).getMarca());
            System.out.printf(format, "Departamento:", stockList.get(i).getDepartamento());

            System.out.printf("ID:\t" + stockList.get(i).getId() + "\n");
            System.out.printf("Descripción:\t" + stockList.get(i).getDescripcion() + "\n");
            System.out.printf("Cantidad:\t" + stockList.get(i).getCantidad() + "\n");
            System.out.printf("Precio:\t" + stockList.get(i).getPrecio() + "\n");
            System.out.printf("Marca:\t" + stockList.get(i).getMarca() + "\n");
            System.out.printf("Departamento:\t" + stockList.get(i).getDepartamento() + "\n");

            System.out.printf("\n- - - - - - - - - - - - \n");
        }

        System.out.printf("\n/******************************************************************************/\n\n");
        return 0;
    }

    /**
     * <p>
     * Lee los datos de un archivo CSV y los carga en una lista.
     * @param csvFile Archivo CSV a leer.
     * @return Una lista de arrays string con las líneas del archivo CSV.
     * @throws IOException
     */
    public static List<String[]> readData(String csvFile) throws IOException {
        File f = new File(csvFile);
        List<String[]> content = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                content.add(line.split(","));
            }
        } catch (FileNotFoundException e) {
            //Some error logging
        }
        return content;
    }

    /**
     * <p>
     * Lee los datos de un archivo CSV y lo imprime en pantalla con un formato establecido.
     * @param csvFile Archivo CSV a leer.
     * @throws IOException
     */
    private static void showCSV(String csvFile) throws IOException {
        List<String[]> datos = readData(csvFile);

        String format = "%-15s%-15s%-65s%-18s%-15s%-15s%n";

        System.out.printf("\n\n/******************************************************************************/\n");
        for (String[] strings : datos) {
            System.out.printf(format, strings[0], strings[1], strings[2], strings[3], strings[4], strings[5]);
        }

        System.out.printf("\n/******************************************************************************/\n\n");
    }

    /**
     * <p>
     * Lee los datos de un archivo CSV y los carga en una lista.
     * @return Lee un archivo CSV y lo carga como almacen del progama.
     * @throws IOException
     */
    private static void createStockCSV() throws IOException {
        ArrayList<Producto> stockList = new ArrayList<>();
        Producto producto1 = new Producto(1, 450, "Aspiradora 840hp", "Koblenz", 5, "Electrodomésticos");
        Producto producto7 = new Producto(2, 1450, "Aspiradora 240hp", "Koblenz", 3, "Electrodomésticos");
        Producto producto8 = new Producto(3, 4250, "Cafetera Digital 12 Tazas Acero Inoxidable", "Hamilton Beach", 2, "Electrodomésticos");
        Producto producto3 = new Producto(4, 4530, "Espumador de Leche Acero Inoxidable", "Krups", 2, "Electrodomésticos");
        Producto producto4 = new Producto(5, 1850, "Máquina de palomitas Star Wars Estrella de la Muerte Plata", "Star Wars", 1, "Electrodomésticos");
        Producto producto5 = new Producto(6, 1950, "Máquina para Donas Azul", "Taurus", 1, "Electrodomésticos");
        Producto producto2 = new Producto(7, 3450, "Fuente de Sodas Oster Roja", "Oster", 2, "Electrodomésticos");
        Producto producto6 = new Producto(8, 4850, "Fábrica de Pan Hamilton Beach Blanca", "Hamilton Beach", 3, "Electrodomésticos");
        Producto producto9 = new Producto(9, 1150, "Fuente de Sodas Hamilton Beach Drink Master Roja Modelo 750RC", "Hamilton Beach", 10, "Electrodomésticos");
        Producto producto10 = new Producto(10, 2450, "Máquina de Palomitas de Maíz Dace Retro Roja", "Nostalgia", 2, "Electrodomésticos");

        stockList.add(producto1);
        stockList.add(producto2);
        stockList.add(producto3);
        stockList.add(producto4);
        stockList.add(producto5);
        stockList.add(producto6);
        stockList.add(producto7);
        stockList.add(producto8);
        stockList.add(producto9);
        stockList.add(producto10);

        String csvFile = new File("stocklist.csv").getAbsolutePath();
        FileWriter writer = new FileWriter(csvFile);

        for (int i = 0; i < stockList.size(); i++) {

            CSVUtils.writeLine(writer, Arrays.asList(Integer.toString(stockList.get(i).getId()), Integer.toString((int) stockList.get(i).getPrecio()), stockList.get(i).getDescripcion(), stockList.get(i).getMarca(), Integer.toString(stockList.get(i).getCantidad()), stockList.get(i).getDepartamento()));
        }
        writer.flush();
        writer.close();
    }

    static ArrayList<Producto> convertCSVtoStockList(String path) throws IOException {
        List<String[]> datos = readData(path);
        ArrayList<Producto> stockList = new ArrayList<Producto>();

        for (String[] strings : datos) {
            stockList.add(new Producto(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]), strings[2], strings[3], Integer.parseInt(strings[4]), strings[5]));

        }

        return stockList;
    }

    private static void fileValidation(String csvFile) {
        File tmp = new File(csvFile);
        if (tmp.isDirectory()) {
            System.out.println(csvFile + " es una carpeta!");
            System.exit(0);
        }
        boolean exists = tmp.exists();
        if (!exists) {
            System.out.println(csvFile + " no existe!");
            System.exit(0);
        }
    }

    private static void saveCarts() throws IOException {
        FileWriter writer = new FileWriter("cartlist.csv");
        for (int i = 0; i < cartList.size(); i++) {

            for (int j = 0; j < cartList.get(i).getItems().size(); j++) {
                CSVUtils.writeLine(writer, Arrays.asList(Integer.toString(i), Integer.toString(cartList.get(i).getItems().get(j).getIdProd()), Integer.toString(cartList.get(i).getItems().get(j).getCantidad())));
            }

        }
        writer.flush();
        writer.close();
    }

    /**
     * <p>
     * Carga la información de los carritos desde el archivo CSV, si no existe crea uno por default, 5 objetos Carrito. En caso de exister un archivo de carrito lo lee.
     * @return ArrayList de los carritos, con sus respectivos items.
     * @throws IOException
     */
    private static ArrayList<CarritoCompra> loadCarts() throws IOException {
        ArrayList<CarritoCompra> cart = new ArrayList<CarritoCompra>();
        File tmp = new File("cartlist.csv");
        boolean exists = tmp.exists();

        if (!exists) {
            ArrayList<ITEM> itemList1 = new ArrayList<ITEM>();
            ArrayList<ITEM> itemList2 = new ArrayList<ITEM>();
            ArrayList<ITEM> itemList3 = new ArrayList<ITEM>();

            for (int i = 0; i < stockList.size(); i++) {
                itemList1.add(new ITEM(stockList.get(i).getId(), stockList.get(i).getCantidad()));
                if (i < 6) {
                    itemList2.add(new ITEM(stockList.get(i).getId(), stockList.get(i).getCantidad()));
                }
            }
            CarritoCompra car1 = new CarritoCompra(itemList1, 1);
            CarritoCompra car2 = new CarritoCompra(itemList2, 2);
            CarritoCompra car3 = new CarritoCompra(itemList3, 3);
            CarritoCompra car4 = new CarritoCompra(itemList1, 4);
            CarritoCompra car5 = new CarritoCompra(itemList2, 5);
            cart.add(car1);
            cart.add(car2);
            cart.add(car3);
            cart.add(car4);
            cart.add(car5);
        } else {
            File f = new File("cartlist.csv");
            List<String[]> content = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    content.add(line.split(","));
                }
            } catch (FileNotFoundException e) {
                //Some error logging
            }

            int a = 0, b = 0;
            for (String[] strings : content) {
                b = Integer.parseInt(strings[0]);
                if (b >= a) {
                    a = b; // a es el numero mayor indice mayor
                }
            }

            for (int j = 0; j <= a; j++) {
                ArrayList<ITEM> itemList = new ArrayList<ITEM>();
                CarritoCompra cartemp = new CarritoCompra(itemList, j + 1);
                cart.add(cartemp);
            }

            for (String[] strings : content) {
                cart.get(Integer.parseInt(strings[0])).agregarProducto(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]));
            }
        }
        return cart;
    }

}
