/**
* @(#)programa4.java
*
*
* @author Jhanno Fouls Lestrange
* @version 1.00 2007/8/7
*/
/** Encontrar los fragmentos de código involucrados en UnNombreDeTipo y
*	convertirlos en un programa que compile y ejecute.
*/
public class programa5 {

   public static void main(String[] args) {

       String nombre="Fernando";

       System.out.println("Bienvenido "+nombre);
   }
}
